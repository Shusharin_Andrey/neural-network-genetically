package com.company.utils

class Utils {
    companion object {
        const val WIN_COUNT = 2.0

        const val LOSS_COUNT = 0.0

        const val DRAW_COUNT = 0.5

        const val FREEDOM_OF_MOVEMENT_COUNT = 0.017

        const val FREEDOM_OF_MOVEMENT_COUNT_ENEMY = -0.1
    }

}