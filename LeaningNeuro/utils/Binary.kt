package com.company.utils

import kotlin.math.max
import kotlin.math.pow

private const val QUANTITY_SIGNS = 5.0
private val SIGNS = 10.0.pow(QUANTITY_SIGNS)
private val SIGNS1 = 10.0.pow(-QUANTITY_SIGNS)

internal object Binary {
    fun binary(dex1: Double): String {
        return Integer.toBinaryString((dex1 * SIGNS).toInt()).reversed()
    }

    @JvmStatic
    fun crossingover(one: Double, two: Double): Double {
        var binOne = binary(one)
        var binTwo = binary(two)

        val length = max(binOne.length, binTwo.length)
        binOne = fillNullToLength(binOne, length)
        binTwo = fillNullToLength(binTwo, length)

        val m = ArrayList<Int>()
        for (i in 0..getRandomInt(length)) {
            m.add(getRandomInt(length))
        }

        m.sort()

//        println(m)
        var summary = ""
        var last = 0
        var isFirst = true;
        for (i in m) {
            summary += if (isFirst) {
                binOne.substring(last, i)
            } else {
                binTwo.substring(last, i)
            }
            isFirst = !isFirst
            last = i
        }
        summary += if (isFirst) {
            binOne.substring(last)
        } else {
            binTwo.substring(last)
        }
        return unBinary(summary)
    }

    private fun fillNullToLength(binOne: String, length: Int): String {
        var binOne1 = binOne
        if (binOne1.length < length) {
            binOne1 += "0".repeat(length - binOne1.length)
        }
        return binOne1
    }

    fun unBinary(two: String): Double {
        var k = 0
        for (i in two.length - 1 downTo 0) {
            k += ((two[i].code - "0"[0].code) * 2.0.pow(i.toDouble())).toInt()
        }
        return k * SIGNS1
    }

    internal fun getRandomInt(max: Int): Int {
        return (Math.random() * max).toInt()
    }

    @JvmStatic
    fun mutation(dex: Double, numberMutation: Int): Double {
        var two = binary(dex)
        var i: Int
        if (getRandomInt(20) == 0) {
            two = "0"
        } else {
            for (j in 0 until numberMutation) {
                val isMore = getRandomInt(20) == 0

                i = getRandomInt(two.length)

                val p: Int = if (two[i].code == 48) {
                    1
                } else {
                    0
                }

                two = two.substring(0, i) + p + two.substring(i + 1) + if (isMore) "1" else ""
            }
        }
        return unBinary(two)
    }
}