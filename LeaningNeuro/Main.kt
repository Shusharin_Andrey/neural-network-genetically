package com.company

import com.company.bots.Bot
import com.company.bots.BotRandom
import com.company.engine.Table
import com.company.neural_network.NeuralNetwork
import com.company.utils.Utils.Companion.DRAW_COUNT
import com.company.utils.Utils.Companion.FREEDOM_OF_MOVEMENT_COUNT
import com.company.utils.Utils.Companion.FREEDOM_OF_MOVEMENT_COUNT_ENEMY
import com.company.utils.Utils.Companion.LOSS_COUNT
import com.company.utils.Utils.Companion.WIN_COUNT
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

private const val QUANTITY_SAVE = 20

private const val NUMBER_NETWORK = 1200
private const val NUMBER_RANDOM = 0
private const val NUMBER_BOT = 0
private const val NUMBER_EPOCH = 10000000
private val PERCEPTRON_NUMBER = intArrayOf(82, 9, 81)

object Main {
    private var networks: MutableList<NeuralNetwork> = ArrayList()

    private fun battleNetworks() {
        val myService: ExecutorService =
            Executors.newFixedThreadPool(10)
        for (i in 0 until NUMBER_NETWORK) {
            for (j in 0 until NUMBER_NETWORK) {
                if (i != j) {
                    myService.submit {
                        battleTwoNetworks(networks[i], networks[j])
                    }
                }
            }
            for (j in 0 until NUMBER_RANDOM) {
                myService.submit {
                    battleNetworksAndRandom(networks[i], true)
                }
                myService.submit {
                    battleNetworksAndRandom(networks[i], false)
                }
            }

            for (j in 0 until NUMBER_BOT) {
                myService.submit {
                    battleNetworksAndBot(networks[i], true)
                }
                myService.submit {
                    battleNetworksAndBot(networks[i], false)
                }
            }
        }

        myService.shutdown();
        try {
            myService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (e: InterruptedException) {
            println(e.message)
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println("Start")
        println(LocalDateTime.now())
        createNetworks(PERCEPTRON_NUMBER)
        networks[0].read("F://finish/3/neuralNetworkFirst0numberEpoch160Count82.88054334798242.txt") //1

        networks[1].read("F://finish/0/neuralNetworkFirst0.txt")//2

        networks[2].read("F://finish/4/neuralNetworkFirstnumberEpoch270Count82.5.txt")//3

        networks[3].read("F://finish/5/neuralNetworkFirstnumberEpoch1200Count80.55555555555556.txt")//4

        networks[4].read("F://finish/1/neuralNetworkFirst0numberEpoch100Count98.67021276595744.txt")//5

        networks[5].read("F://finish/6/neuralNetworkFirst = truenumberEpoch160Count82.85714285714286.txt")//6

        networks[6].read("F://finish/7/neuralNetworkFirst = truenumberEpoch160Count81.18811881188118.txt")//7

        networks[7].read("F://finish/8/neuralNetworknumberEpoch38.txt")//8

        networks[8].read("F://finish/9/neuralNetworkFirst=truenumberEpoch3910Count93.18181818181819.txt")//9

        networks[9].read("F://finish/10/neuralNetworknumberEpoch800.txt")//10

        networks[10].read("F://finish/11/neuralNetworkFirst=truenumberEpoch500.txt")//11

        networks[11].read("F://finish/12/neuralNetworkFirst=truenumberEpoch270.txt")//12

        networks[12].read("F://finish/13/neuralNetworknumberEpoch40.txt")//13
        for (j in 0..NUMBER_EPOCH) {
            battleNetworks()
            networks.sortDescending()
            if (j % 10 == 0 || j == 1) {
                println(j)
                printN(j, networks, QUANTITY_SAVE)
                println(LocalDateTime.now())
            }
            val neuralNetworksTimeFirst = ArrayList<NeuralNetwork>()
            while (neuralNetworksTimeFirst.size < NUMBER_NETWORK) {
                for (i in 0..QUANTITY_SAVE) {
                    fill(neuralNetworksTimeFirst, i, networks, networks[i])
                    if ((neuralNetworksTimeFirst.size == NUMBER_NETWORK)) break
                }
            }
            networks.clear()
            networks.addAll(neuralNetworksTimeFirst)
        }
        var i = 0
        while (i < 10) {
            networks[i].print("F://neuralNetworkFirst$i.txt")
            i += 3
        }
    }

    private fun printN(j: Int, neuralNetworks: MutableList<NeuralNetwork>, n: Int) {
        for (m in 0 until n) {
            print("№$m ")
            printInfo(neuralNetworks[m], j)
        }
    }

    private fun printInfo(neuralNetwork: NeuralNetwork, epoch: Int) {
        println("ID = " + neuralNetwork.id + " " + neuralNetwork.countAll.toString() + " " + neuralNetwork.percentWinSummary.toString() + " " + neuralNetwork.percentWin + " " + neuralNetwork.percentWinRandom)
        neuralNetwork.print("F://neuralNetworknumberEpoch$epoch.txt")
    }

    private fun fill(
        neuralNetworksTimeFirst: ArrayList<NeuralNetwork>,
        i: Int,
        neuralNetworks: List<NeuralNetwork>,
        neuralNetwork: NeuralNetwork
    ) {
        neuralNetwork.nullCount()
        if (neuralNetworksTimeFirst.size < QUANTITY_SAVE) {
            neuralNetworksTimeFirst.add(neuralNetwork)
        } else {
            if (neuralNetworksTimeFirst.size < NUMBER_NETWORK) {
                neuralNetworksTimeFirst.add(
                    NeuralNetwork(
                        neuralNetwork
                    )
                )
            }
            if (neuralNetworksTimeFirst.size < NUMBER_NETWORK) {
                var idNew = (Math.random() * NUMBER_NETWORK).toInt()
                if (i == idNew) {
                    if (NUMBER_NETWORK - 1 > idNew) {
                        idNew++
                    } else {
                        idNew--
                    }
                }
                neuralNetworksTimeFirst.add(
                    NeuralNetwork(
                        neuralNetwork,
                        neuralNetworks[idNew]
                    )
                )
            }
            if (neuralNetworksTimeFirst.size < NUMBER_NETWORK) {
                networks.add(NeuralNetwork(PERCEPTRON_NUMBER))
            }
        }
    }

    private fun createNetworks(perceptronNumber: IntArray) {
        for (i in 0 until NUMBER_NETWORK) {
            networks.add(NeuralNetwork(perceptronNumber))
        }
    }


    private fun battleTwoNetworks(neuralNetwork1: NeuralNetwork, neuralNetwork2: NeuralNetwork) {
        val table = Table()
        val list = DoubleArray(82)
        var countMove = 0
        while (!table.isFinished) {
            if (table.isFirst) {
                makeMotion(table, list, true)
                checkMethod(neuralNetwork1, list, table)
            } else {
                makeMotion(table, list, true)
                checkMethod(neuralNetwork2, list, table)
            }
            countMove++
        }
        when (table.whatToPaintOverBig) {
            1 -> {
                neuralNetwork1.count = WIN_COUNT
                neuralNetwork1.setEncouragement(getCountForMove(countMove))
                neuralNetwork2.count = LOSS_COUNT
            }

            2 -> {
                neuralNetwork2.count = WIN_COUNT
                neuralNetwork2.setEncouragement(getCountForMove(countMove))
                neuralNetwork1.count = LOSS_COUNT
            }

            else -> {
                neuralNetwork1.count = DRAW_COUNT
                neuralNetwork2.count = DRAW_COUNT
            }
        }
    }

    @JvmStatic
    fun getCountForMove(countMove: Int): Double {
        val countMove = countMove - 17
        return 1.0 - countMove / 64.0
    }

    private fun makeMotion(table: Table, list: DoubleArray, isFirst: Boolean) {
        for (i in 0..8) {
            for (j in 0..8) {
                var value = table.getSmallSquare(i, j)
                if (isFirst) {
                    list[Table.get_number(i, j)] = value.toDouble()
                } else {
                    if (value == 1) {
                        value = 2
                    } else if (value == 2) {
                        value = 1
                    }
                    list[Table.get_number(i, j)] = value.toDouble()
                }
            }
        }
        if (table.getBigSquareInt(table.whereToPoke) == 0) {
            list[81] = table.whereToPoke.toDouble()
        } else {
            list[81] = 10.0
        }
    }

    private fun battleNetworksAndRandom(neuralNetwork: NeuralNetwork, isFirst: Boolean) {
        val table = Table()
        val list = DoubleArray(82)
        var countMove = 0
        while (!table.isFinished) {
            if (isFirst == table.isFirst) {
                makeMotion(table, list, isFirst)
                checkMethod(neuralNetwork, list, table)
            } else {
                makeMotion(BotRandom.randomMotion(table), table)
            }
            countMove++
        }

        val win = table.whatToPaintOverBig
        val needWin = if (isFirst) 1 else 2
        when (win) {
            needWin -> {
                neuralNetwork.countRandom = WIN_COUNT
                neuralNetwork.setEncouragement(getCountForMove(countMove))
            }

            3 -> {
                neuralNetwork.countRandom = DRAW_COUNT
            }

            else -> {
                neuralNetwork.countRandom = LOSS_COUNT
            }
        }
    }

    private fun battleNetworksAndBot(neuralNetwork: NeuralNetwork, isFirst: Boolean) {
        val table = Table()
        val list = DoubleArray(82)
        var countMove = 0
        while (!table.isFinished) {
            if (isFirst == table.isFirst) {
                makeMotion(table, list, isFirst)
                checkMethod(neuralNetwork, list, table)
            } else {
                if (isFirst) {
                    makeMotion(Bot.botMotion(table, 2, 1), table)
                } else {
                    makeMotion(Bot.botMotion(table, 1, 2), table)
                }
            }
            countMove++
        }

        val win = table.whatToPaintOverBig
        val needWin = if (isFirst) 1 else 2
        when (win) {
            needWin -> {
                neuralNetwork.countRandom = WIN_COUNT
                neuralNetwork.setEncouragement(getCountForMove(countMove))
            }

            3 -> {
                neuralNetwork.countRandom = DRAW_COUNT
            }

            else -> {
                neuralNetwork.countRandom = LOSS_COUNT
            }
        }
    }

    private fun checkMethod(neuralNetwork: NeuralNetwork, list: DoubleArray, table: Table) {
        val ij = neuralNetwork.getAction(list)
        if (table.getBigSquareInt(table.whereToPoke) != 0) {
            neuralNetwork.setEncouragement(FREEDOM_OF_MOVEMENT_COUNT)
        }
        makeMotion(ij, table)
        if (table.getBigSquareInt(table.whereToPoke) != 0) {
            neuralNetwork.setEncouragement(FREEDOM_OF_MOVEMENT_COUNT_ENEMY)
        }
    }

    private fun makeMotion(ij: IntArray, table: Table) {
        val i = ij[0]
        val j = ij[1]
        if (table.getSmallSquare(i, j) != 0) {
            println("Хрень")
        }
        table.setSmallSquare(i, j, if (table.isFirst) 1 else 2)
        table.invertFirst()
        if (table.eraseOrFillBigSquare(i, j)) {
            if (table.beFinal()) {
                table.isFinished = true
            }
        }
        table.whereToPoke = Table.whereDrawFrame(i, j)
    }
}