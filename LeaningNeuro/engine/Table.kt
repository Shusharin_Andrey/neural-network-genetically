package com.company.engine

class Table internal constructor() {
    private val smallSquare = Array(9) { IntArray(9) }
    private val bigSquareInt = IntArray(9)
    var whereToPoke = 4
    var isFinished = false
    var isFirst = true
        private set
    var whatToPaintOverBig = 0
        private set
    private var pokeIt = 0
    fun getSmallSquare(i: Int, j: Int): Int {
        return smallSquare[i][j]
    }

    fun setSmallSquare(i: Int, j: Int, k: Int) {
        smallSquare[i][j] = k
    }

    fun getBigSquareInt(i: Int): Int {
        return bigSquareInt[i]
    }

    fun setBigSquareInt(i: Int, k: Int) {
        bigSquareInt[i] = k
    }

    fun invertFirst() {
        isFirst = !isFirst
    }

    fun eraseOrFillBigSquare(i: Int, j: Int): Boolean {
        pokeIt = whereDidPokeIt(i, j)
        val x = pokeIt % 3
        val y = pokeIt / 3
        val m = intArrayOf(
            smallSquare[y * 3][x * 3],
            smallSquare[y * 3][1 + x * 3],
            smallSquare[y * 3][2 + x * 3],
            smallSquare[1 + y * 3][x * 3],
            smallSquare[1 + y * 3][1 + x * 3],
            smallSquare[1 + y * 3][2 + x * 3],
            smallSquare[2 + y * 3][x * 3],
            smallSquare[2 + y * 3][1 + x * 3],
            smallSquare[2 + y * 3][2 + x * 3]
        )
        whatToPaintOverBig = checkMiddleCell(m)
        val q: Int
        val w: Int
        if (whatToPaintOverBig != 0) {
            bigSquareInt[pokeIt] = whatToPaintOverBig
            q = 0
            w = 4
            for (l in 0..2) {
                for (n in 0..2) {
                    if (smallSquare[l + y * 3][n + x * 3] == q) {
                        smallSquare[l + y * 3][n + x * 3] = w
                    }
                }
            }
            return true
        }
        return false
    }

    fun beFinal(): Boolean {
        whatToPaintOverBig = checkMiddleCell(bigSquareInt)
        return whatToPaintOverBig != 0
    }

    private fun checkMiddleCell(m: IntArray): Int {
        var t = 0
        if (m[0] * m[1] * m[2] * m[3] * m[4] * m[5] * m[6] * m[7] * m[8] != 0) {
            t = 3
        }
        if (m[0] != 0) {
            if (m[0] == m[1] && m[0] == m[2]) {
                t = m[0]
            } else if (m[0] == m[3] && m[0] == m[6]) {
                t = m[0]
            } else if (m[0] == m[4] && m[0] == m[8]) {
                t = m[0]
            }
        }
        if (m[4] != 0) {
            if (m[3] == m[4] && m[3] == m[5]) {
                t = m[3]
            } else if (m[1] == m[4] && m[1] == m[7]) {
                t = m[1]
            } else if (m[2] == m[4] && m[2] == m[6]) {
                t = m[2]
            }
        }
        if (m[8] != 0) {
            if (m[6] == m[7] && m[6] == m[8]) {
                t = m[6]
            } else if (m[2] == m[5] && m[2] == m[8]) {
                t = m[2]
            }
        }
        return t
    }

    companion object {
        fun whereDrawFrame(i: Int, j: Int): Int {
            var i = i
            var j = j
            i %= 3
            j %= 3
            return getK(i, j)
        }

        fun whereDidPokeIt(i: Int, j: Int): Int {
            var i = i
            var j = j
            i /= 3
            j /= 3
            return getK(i, j)
        }

        fun getK(i: Int, j: Int): Int {
            return 3 * i + j
        }

        @JvmStatic
        fun getK(ij: IntArray): Int {
            return whereDidPokeIt(ij[0], ij[1])
        }

        @JvmStatic
        fun get_i_j(number: Int): IntArray {
            return intArrayOf(number / 9, number % 9)
        }

        fun get_number(i: Int, j: Int): Int {
            return i * 9 + j
        }
    }
}