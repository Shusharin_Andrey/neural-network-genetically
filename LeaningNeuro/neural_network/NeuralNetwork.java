package com.company.neural_network;

import com.company.engine.Table;
import org.jetbrains.annotations.NotNull;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static com.company.utils.Utils.WIN_COUNT;
import static java.lang.Math.abs;

public class NeuralNetwork implements Comparable<NeuralNetwork> {
    private final Perceptron[][] perceptrons;
    private final double eta = 0.15;
    private double count = 0;

    static int ID = 0;
    String prefix = "_";
    String parentPrefix = "";
    private int id = 0;

    public double getCountRandom() {
        return countRandom;
    }

    private double countRandom = 0;
    private double countEncouragement = 0;
    private int quantityGame = 0;
    private int quantityGameRandom = 0;

    private void init(String prefix) {
        ID++;
        id = ID;
        this.prefix = prefix;
    }

    public NeuralNetwork(int[] perceptronNumber) {
        init("create");
        count = 0;
        if (perceptronNumber.length == 0) {
            throw new IllegalArgumentException("wrong number of layers");
        }
        int inputCount = perceptronNumber[0];

        if (inputCount <= 0) {
            throw new IllegalArgumentException("wrong number of inputs");
        }
        for (int value : perceptronNumber) {
            if (value <= 0) {
                throw new IllegalArgumentException("wrong number of perceptrons in a layer");
            }
        }
        perceptrons = new Perceptron[perceptronNumber.length][];
        for (int i = 0; i < perceptronNumber.length; i++) {
            perceptrons[i] = new Perceptron[perceptronNumber[i]];
            int num = (i == 0) ? inputCount : perceptronNumber[i - 1];
            for (int j = 0; j < perceptrons[i].length; j++) {
                perceptrons[i][j] = new Perceptron(num);
            }
        }
    }

    public NeuralNetwork(NeuralNetwork neuralNetwork1, NeuralNetwork neuralNetwork2) {
        parentPrefix = neuralNetwork1.prefix + "_" + neuralNetwork1.id + ", " + neuralNetwork2.prefix + "_" + neuralNetwork2.id;
        init("crossingover");
        count = 0;
        perceptrons = new Perceptron[neuralNetwork1.perceptrons.length][];
        for (int i = 0; i < perceptrons.length; i++) {
            perceptrons[i] = new Perceptron[neuralNetwork1.perceptrons[i].length];
            for (int j = 0; j < perceptrons[i].length; j++) {
                perceptrons[i][j] = new Perceptron(neuralNetwork1.perceptrons[i][j], neuralNetwork2.perceptrons[i][j]);
            }
        }
    }

    public NeuralNetwork(NeuralNetwork neuralNetwork1) {
        parentPrefix = neuralNetwork1.prefix + "_" + neuralNetwork1.id;
        init("mutation");
        count = 0;
        perceptrons = new Perceptron[neuralNetwork1.perceptrons.length][];
        for (int i = 0; i < perceptrons.length; i++) {
            perceptrons[i] = new Perceptron[neuralNetwork1.perceptrons[i].length];
            for (int j = 0; j < perceptrons[i].length; j++) {
                perceptrons[i][j] = new Perceptron(neuralNetwork1.perceptrons[i][j]);
            }
        }
    }


    public boolean equals(NeuralNetwork obj) {
        for (int i = 0; i < perceptrons.length; i++) {
            for (int j = 0; j < perceptrons[i].length; j++) {
                if (!perceptrons[i][j].equals(obj.perceptrons[i][j])) {
                    System.out.println(i + " " + j + " " + perceptrons[i][j] + " " + obj.perceptrons[i][j]);
                    return false;
                }
            }
        }
        return true;
    }

    public double calculateCountNotEqual(NeuralNetwork obj) {
        var count = 0.0;
        for (int i = 0; i < perceptrons.length; i++) {
            for (int j = 0; j < perceptrons[i].length; j++) {
                count += perceptrons[i][j].calculateCountNotEqual(obj.perceptrons[i][j]);
            }
        }
        return count;
    }

    public double calculateSum() {
        var count = 0.0;
        for (int i = 0; i < perceptrons.length; i++) {
            for (int j = 0; j < perceptrons[i].length; j++) {
                count += abs(perceptrons[i][j].calculateSum());
            }
        }
        return count;
    }

    private double[][] calculateOutputs(double[] inputs) {
        final double[][] result = new double[perceptrons.length][];
        for (int i = 0; i < perceptrons.length; i++) {
            result[i] = new double[perceptrons[i].length];
            for (int j = 0; j < perceptrons[i].length; j++) {
                final double[] in = (i == 0) ?
                        inputs : result[i - 1];
                result[i][j] = perceptrons[i][j].getOutput(in);
            }
        }
        return result;
    }

    public int[] getAction(double[] inputs) {
        double[] output = getOutput(inputs);
        int i_max = 0;
        double max = -1.7E+308;
        for (int i = 0; i < output.length; i++) {
            if ((Table.getK(Table.get_i_j(i)) == inputs[81] || inputs[81] == 10) && inputs[i] == 0) {
                if (output[i] >= max) {
                    i_max = i;
                    max = output[i];
                }
            }
        }
        return Table.get_i_j(i_max);
    }

    public double[] getOutput(double[] inputs) {
        if (inputs.length != perceptrons[0][0].getInputCount()) {
            System.out.println(inputs.length + " " + perceptrons[0][0].getInputCount());
            throw new IllegalArgumentException("wrong number of inputs");
        }
        return calculateOutputs(inputs)[perceptrons.length - 1];
    }

    public void nullCount() {
        this.count = 0;
        this.countRandom = 0;
        this.quantityGame = 0;
        this.quantityGameRandom = 0;
        this.countEncouragement = 0;
    }

    public double getCount() {
        return count;
    }

    public double getCountSummary() {
        return count + countRandom;
    }

    public double getCountAll() {
        return count + countRandom + countEncouragement;
    }

    public double getPercentWin() {
        return count * 100 / (WIN_COUNT * quantityGame);
    }

    public double getPercentWinRandom() {
        return countRandom * 100 / (WIN_COUNT * quantityGameRandom);
    }

    public double getPercentWinSummary() {
        return getCountSummary() * 100 / (WIN_COUNT * getQuantityGameSummary());
    }

    public void setCount(double count) {
        this.count += count;
        quantityGame++;
    }

    public void setEncouragement(double count) {
        this.countEncouragement += count;
    }

    public int getQuantityGameSummary() {
        return quantityGame + quantityGameRandom;
    }

    public void setCountRandom(double count) {
        this.countRandom += count;
        quantityGameRandom++;
    }

    public void print(String name) {
        try (FileWriter writer = new FileWriter(name, false)) {
            for (Perceptron[] perceptron : perceptrons) {
                for (Perceptron value : perceptron) {
                    for (int k = 0; k < value.getInputCount(); k++) {
                        writer.append(String.valueOf(value.getWeight(k)));
                        writer.append("\n");
                    }
                }
            }
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void read(String name) {
        prefix = "read";
        try (FileReader reader = new FileReader(name)) {
            // читаем посимвольно
            int c;
            for (Perceptron[] perceptron : perceptrons) {
                for (Perceptron value : perceptron) {
                    for (int k = 0; k < value.getInputCount(); k++) {
                        StringBuilder weightString = new StringBuilder();
                        while ((c = reader.read()) != -1) {
                            char symbol = (char) c;
                            if (symbol == '\n') {
//                                System.out.println(weightString);
                                double weight = Double.parseDouble(weightString.toString());
                                value.setWeight(k, weight);
//                                System.out.println(weight);
                                break;
                            } else {
                                weightString.append(symbol);
                            }
                        }
                    }
                }

            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public int compareTo(@NotNull NeuralNetwork o) {
        return Double.compare(getCountAll(), o.getCountAll());
    }

    public String getId() {
        if (parentPrefix.equals("")) {
            return prefix + "_" + id;
        } else {
            return prefix + "(" + parentPrefix + ")_" + id;
        }
    }
}
