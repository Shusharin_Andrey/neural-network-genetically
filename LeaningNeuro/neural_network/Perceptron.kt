package com.company.neural_network

import com.company.utils.Binary
import com.company.utils.Binary.crossingover
import com.company.utils.Binary.mutation
import kotlin.math.abs
import kotlin.math.exp

internal class Perceptron {
    private val weights: DoubleArray

    constructor(inputCount: Int) {
        weights = DoubleArray(inputCount)
        for (i in weights.indices) {
            weights[i] = Math.random()
        }
    }

    constructor(perceptron1: Perceptron, perceptron2: Perceptron) {
        weights = DoubleArray(perceptron1.weights.size)
        for (i in weights.indices) {
            weights[i] = crossingover(perceptron1.weights[i], perceptron2.weights[i])
        }
    }

    constructor(perceptron1: Perceptron) {
        weights = DoubleArray(perceptron1.weights.size)
        for (i in weights.indices) {
            if(Binary.getRandomInt(10) == 0) {
                weights[i] = mutation(perceptron1.weights[i], (Math.random() * 2).toInt())
            }
        }
    }

    fun getInputCount(): Int {
        return weights.size
    }

    fun getWeight(i: Int): Double {
        return weights[i]
    }

    fun setWeight(i: Int, value: Double) {
        weights[i] = value
    }

    fun getOutput(input: DoubleArray): Double {
        var sum = 0.0
        for (i in input.indices) {
            sum += weights[i] * input[i]
        }
        return 1.0 / (1.0 + exp(-sum))
    }

    fun equals(obj: Perceptron): Boolean {
        for (i in weights.indices) {
            if (weights[i] != obj.weights[i]) {
                println(i.toString() + " " + " " + weights[i] + " " + obj.weights[i])
                return false
            }
        }
        return true
    }

    fun calculateCountNotEqual(obj: Perceptron): Double {
        var count = 0.0
        for (i in weights.indices) {
            count += abs(weights[i] - obj.weights[i])
        }
        return count
    }

    fun calculateSum(): Double {
        var count = 0.0
        for (i in weights.indices) {
            count += abs(weights[i])
        }
        return count
    }
}