package com.company.test

import com.company.neural_network.NeuralNetwork
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test

internal class NeuralNetworkTest {
    @Test
    fun print() {
    }

    @Test
    fun read() {
        val name = "F://testRead.txt"
        val network = NeuralNetwork(intArrayOf(82, 9, 81))
        network.print(name)
        val network1 = NeuralNetwork(intArrayOf(82, 9, 81))
        network1.read(name)
        Assertions.assertTrue(network.equals(network1))
    }

    @Test
    fun read1() {
        val network = NeuralNetwork(intArrayOf(82, 9, 81))
        val network1 = NeuralNetwork(intArrayOf(82, 9, 81))
        network.read("F://finish/1/neuralNetworkFirst0numberEpoch100Count98.67021276595744.txt")
        network1.read("F://finish/1/neuralNetworkSecond0numberEpoch100Count96.61157024793388.txt")
        //        Main.getNeuralNetworksFirst().add(network);
    }

    @RepeatedTest(10)
    fun checkSizeModify() {
        val network = NeuralNetwork(intArrayOf(82, 9, 81))
        val network1 = NeuralNetwork(network)
        println(network.calculateSum())
        println(network1.calculateSum())
        println(network.calculateCountNotEqual(network1))
        println()
    }

    @RepeatedTest(10)
    fun checkSizeModify1() {
        val network = NeuralNetwork(intArrayOf(82, 9, 81))
        val network1 = NeuralNetwork(intArrayOf(82, 9, 81))
        val network2 = NeuralNetwork(network,network1)
        println(network.calculateSum())
        println(network1.calculateSum())
        println(network2.calculateSum())
        println()
        println(network.calculateCountNotEqual(network1))
        println(network.calculateCountNotEqual(network2))
        println(network1.calculateCountNotEqual(network2))
        println()
        println()
        println()
    }

    @Test
    fun checkSizeModify2() {
        var network = NeuralNetwork(intArrayOf(82, 9, 81))
        var network1 = NeuralNetwork(intArrayOf(82, 9, 81))
        println(network.calculateSum())
        println(network1.calculateSum())
        println(network.calculateCountNotEqual(network1))
        println()
        for (i in 0..15){
            val network2 = NeuralNetwork(network,network1)
            val network3 = NeuralNetwork(network,network1)
            network=network2
            network1=network3
            println(network.calculateCountNotEqual(network1))
        }
        println(network.calculateSum())
        println(network1.calculateSum())
        println(network.calculateCountNotEqual(network1))
        println()
    }

    @Test
    fun checkSizeModify3() {
        var network = NeuralNetwork(intArrayOf(82, 9, 81))
        var network1 = network
        println(network.calculateSum())
        println(network1.calculateSum())
        println(network.calculateCountNotEqual(network1))
        println()
        for (i in 0..150){
            network = NeuralNetwork(network)
            println(network.calculateCountNotEqual(network1))
        }

        println()
        println(network.calculateSum())
        println(network1.calculateSum())
        println(network.calculateCountNotEqual(network1))
        println()
    }
}