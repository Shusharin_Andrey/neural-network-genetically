package com.company.test

import com.company.utils.Binary.binary
import com.company.utils.Binary.crossingover
import com.company.utils.Binary.mutation
import com.company.utils.Binary.unBinary
import junit.framework.TestCase.assertEquals
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class BinaryTest {

    @Test
    fun binary() {
        println(binary(10.0))
        println(binary(150.0))
        println(binary(0.0))
    }

    @Test
    fun addBinary() {
        println(binary(10.0))
        println(binary(150.0))
        val one = binary(10.03)
        val two = binary(150.220)
        println(unBinary(one))
        println(unBinary(two))
    }

    @ParameterizedTest(name = "Не изменяется при 2-ух одинаковых значениях")
    @ValueSource(ints = [1, 2, 4, 5])
    fun crossingover(value: Double) {
        assertEquals(value, crossingover(value, value))
    }

    @RepeatedTest(10)
    fun `Проверяем изменение значения`() {
        println(crossingover(10.0, 150.0))
    }

    @RepeatedTest(100)
    fun crossingoverMany() {
        var value = 10.0
        var valueTwo = 150.0
        for (i in 1..100) {
            val valueTemp = crossingover(value, valueTwo)
            valueTwo = crossingover(value, valueTwo)
            value = valueTemp
        }
        println(value)
        println(valueTwo)
        println()
    }

    @RepeatedTest(100)
    fun mutationMany() {
        var value = 1000.0
        for (i in 1..100) {
            value = mutation(value, 1)
        }
        println(value)
    }

    @Test
    fun mutation1() {
        var two = binary(0.04)
        two = "$two" + 1
        println(unBinary(two))
    }

    @RepeatedTest(100)
    fun mutation2() {
        println(mutation(0.2, 1))
    }

    @ParameterizedTest(name = "Не изменяется при 2-ух одинаковых значениях")
    @ValueSource(doubles = [1.0, 2.0, 4.3, 5.5, 1879432.6549745975])
    fun mutation3(dex: Double) {
        println(dex.toBits())
    }

    @Test
    fun random() {
        println(Integer.toBinaryString(4 * 10000))
        println(binary(4.0).reversed())
    }
}