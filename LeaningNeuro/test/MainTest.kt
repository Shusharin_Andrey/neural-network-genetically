package com.company.test

import com.company.Main
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class MainTest {

    @ParameterizedTest(name = "Check")
    @ValueSource(ints = [17, 81, 20, 31, 50, 64, 80])
    fun getCountForMove(count: Int) {
        println(Main.getCountForMove(count))
    }

    @ParameterizedTest(name = "Check")
    @ValueSource(ints = [0, 1, 2])
    fun checkY(y: Int) {
        val smallSquare = IntArray(9) { 0 }
        var current = 0
        for (i in 0..2) {
            for (j in 0..2) {
                smallSquare[current] = i + 3 * y
                current++
            }
        }

        for (index in 0 until current) {
            assertEquals(smallSquare[index],index/3 + y * 3)
        }
    }

    @ParameterizedTest(name = "Check")
    @ValueSource(ints = [0, 1, 2])
    fun checkX(x: Int) {
        val smallSquare = IntArray(9) { 0 }
        var current = 0
        for (i in 0..2) {
            for (j in 0..2) {
                smallSquare[current] = j + 3 * x
                current++
            }
        }

        for (index in 0 until current) {
            assertEquals(smallSquare[index],index%3 + x * 3)
        }
    }
}