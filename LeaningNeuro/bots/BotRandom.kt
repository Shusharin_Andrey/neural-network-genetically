package com.company.bots

import com.company.Table
import java.util.ArrayList
import java.util.Random

object BotRandom {
    private var random = Random()
    @JvmStatic
    fun randomMotion(table: Table): IntArray {
        val arrayListI = ArrayList<Int>()
        val arrayListJ = ArrayList<Int>()
        if (table.getBigSquareInt(table.whereToPoke) != 0) {
            for (i in 0..8) {
                for (j in 0..8) {
                    if (table.getSmallSquare(i, j) == 0) {
                        arrayListI.add(i)
                        arrayListJ.add(j)
                    }
                }
            }
        } else {
            val pokeIt = table.whereToPoke
            val x = pokeIt % 3
            val y = pokeIt / 3
            for (i in 0..2) {
                for (j in 0..2) {
                    if (table.getSmallSquare(i + 3 * y, j + 3 * x) == 0) {
                        arrayListI.add(i + 3 * y)
                        arrayListJ.add(j + 3 * x)
                    }
                }
            }
        }
        val q = random.nextInt(arrayListI.size)
        return intArrayOf(arrayListI[q], arrayListJ[q])
    }
}