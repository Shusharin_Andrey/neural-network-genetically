package com.company.bots

import com.company.engine.Table

object Bot {

    @JvmStatic
    fun botMotion(table: Table, id: Int, idEnemy: Int): IntArray {
        huPlayer = idEnemy
        aiPlayer = id

        val pokeIt = if (table.getBigSquareInt(table.whereToPoke) != 0) {
            val bigSquare = IntArray(9) { 0 }
            for (i in 0..8) {
                bigSquare[i] = table.getBigSquareInt(i)
            }
            minimax(bigSquare, id)
        } else {
            table.whereToPoke
        }
        val x = pokeIt % 3
        val y = pokeIt / 3
        val smallSquare = IntArray(9) { 0 }
        var current = 0
        var currentAi = 0
        var currentHu = 0
        for (i in 0..2) {
            for (j in 0..2) {
                val value = table.getSmallSquare(i + 3 * y, j + 3 * x)
                smallSquare[current] = value
                current++
                if (value == id) {
                    currentAi++
                } else if (value == idEnemy) {
                    currentHu++
                }
            }
        }
//        println(emptyIndexies(smallSquare))
        if (currentAi < 2 && currentHu < 2) {
            if (smallSquare[4] == 0) {
                smallSquare[4] = 3
            }
        }

        val index = minimax(smallSquare, id)
//        println(index)
        // finding the ultimate play on the game that favors the computer
        return intArrayOf(index / 3 + y * 3, index % 3 + x * 3)
    }


    private var huPlayer = -1;

    // ai
    private var aiPlayer = -1;
    fun minimax(newBoardCopy: IntArray, player: Int): Int {
        return minimax(newBoardCopy, player, 0).first
    }

    fun minimax(newBoardCopy: IntArray, player: Int, quantityMove: Int): Pair<Int, Double> {
        val newBoard = newBoardCopy.clone()
        val availSpots = emptyIndexies(newBoard);

        // checks for the terminal states such as win, lose, and tie and returning a value accordingly
        if (winning(newBoard, huPlayer)) {
            return Pair(-1, -10.0)
        } else if (winning(newBoard, aiPlayer)) {
            return Pair(-1, 10.0 / quantityMove)
        } else if (availSpots.isEmpty()) {
            return Pair(-1, 0.0)
        }

        val moves = ArrayList<Pair<Int, Double>>()

        for (index in availSpots) {
            newBoard[index] = player;

            val score = if (player == aiPlayer) {
                minimax(newBoard, huPlayer, quantityMove + 1).second;
            } else {
                minimax(newBoard, aiPlayer, quantityMove + 1).second;
            }

            newBoard[index] = 0

            moves.add(Pair(index, score));
        }

        moves.sortBy { it.second }

        val bestMoves = if (player != aiPlayer) {
            moves.filter { it.second == moves[0].second }
        } else {
            moves.filter { it.second == moves[moves.size - 1].second }
        }

        return bestMoves[(Math.random() * bestMoves.size).toInt()]
    }

    private fun winning(board: IntArray, player: Int): Boolean {
        return (board[0] == player && board[1] == player && board[2] == player) ||
                (board[3] == player && board[4] == player && board[5] == player) ||
                (board[6] == player && board[7] == player && board[8] == player) ||
                (board[0] == player && board[3] == player && board[6] == player) ||
                (board[1] == player && board[4] == player && board[7] == player) ||
                (board[2] == player && board[5] == player && board[8] == player) ||
                (board[0] == player && board[4] == player && board[8] == player) ||
                (board[2] == player && board[4] == player && board[6] == player)
    }


    fun emptyIndexies(board: IntArray): List<Int> {
        val availSpots = ArrayList<Int>()
        for (i in board.indices) {
            if (board[i] == 0) {
                availSpots.add(i)
            }
        }
        return availSpots
    }

}